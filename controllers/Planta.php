<?php

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
ini_set('display_errors', true);

require_once '../dao/RegistoDAO.php';
require_once '../dao/PlantaDAO.php';
require_once '../models/PlantaDTO.php';
require_once '../models/RegistoDTO.php';

//SETAR TIMEZONE
date_default_timezone_set('Europe/Lisbon');
?>

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Planta
 *
 * @author pi
 */
class Planta {

    //ATRIBUTOS
    //GET SET
    //CONSTRUTOR
    public function __construct() {
        
    }

    //METODOS
    public function lerDHT11() {
        $json = exec("sudo python ../scripts/lerDHT11.py 2>&1");

        return $json;
    }

    public function obterHumidadeMin($idPlanta) {
        //INSTANCIAR DAO
        $plantaDAO = new PlantaDAO();

        //OBTER HUMIDADE MIN
        $controlo = $plantaDAO->consultaGenerica("SELECT * FROM t_plantas WHERE id_planta = ?", $idPlanta);
        
        //VER ERROS BD
        //BUSCAR TRANSACAO
        $numeroTransacao = array_column($controlo, "numero_transacao");
        $descricaoTransacao = array_column($controlo, "descricao_transacao");

        if ($numeroTransacao[0] !== 0) {
            //CONVERTER JSON
            return json_encode(array(
                "numero_transacao" => $numeroTransacao[0],
                "descricao_transacao" => $descricaoTransacao[0]
                    ), JSON_UNESCAPED_UNICODE);
        } else {
            //INSTANCIAR MODELO
            $plantaDTO = new PlantaDTO();

            //SETAR MODELO
            $plantaDTO->setIdPlanta($idPlanta);
            $plantaDTO->setNome($controlo[0]["nome"]);
            $plantaDTO->setHumidadeMin($controlo[0]["humidade_min"]);

            //CONVERTER JSON
            return json_encode($plantaDTO->jsonSerialize(), JSON_UNESCAPED_UNICODE);
        }
    }

    public function mudarHumidade($humidade, $idPlanta) {
        if (is_numeric($idPlanta) && is_numeric($humidade)) {
            //INSTANCIAR DAO
            $plantaDAO = new PlantaDAO();

            //MUDAR HUMIDADE MINIMA
            $controlo = $plantaDAO->updateHumidade(
                    "UPDATE t_plantas 
                        SET humidade_min = ?
                    WHERE id_planta = ?",
                    $humidade,
                    $idPlanta);

            //VER ERROS BD
            //BUSCAR TRANSACAO
            if ($controlo["numero_transacao"] !== 0) {
                //CONVERTER JSON
                return json_encode(array(
                    "numero_transacao" => $controlo["numero_transacao"],
                    "descricao_transacao" => $controlo["descricao_transacao"]
                        ), JSON_UNESCAPED_UNICODE);
            } else {
                //INSTANCIAR MODELO
                $plantaDTO = new PlantaDTO();

                //SETAR MODELO
                $plantaDTO->setIdPlanta($idPlanta);
                $plantaDTO->setHumidadeMin($humidade);

                //CONVERTER JSON
                return json_encode($plantaDTO->jsonSerialize(), JSON_UNESCAPED_UNICODE);
            }
        }
    }
    
    public function regaManual($idPlanta, $segundos){
        if (is_numeric($idPlanta) && is_numeric($segundos)) {
            
            $result = shell_exec('sudo python ../scripts/regaManual.py _' . $idPlanta . "_" . $segundos);
        }
    }

}
