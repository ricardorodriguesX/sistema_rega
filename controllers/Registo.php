<?php

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
ini_set('display_errors', true);

require_once '../dao/RegistoDAO.php';
require_once '../dao/PlantaDAO.php';
require_once '../models/PlantaDTO.php';
require_once '../models/RegistoDTO.php';

//SETAR TIMEZONE
date_default_timezone_set('Europe/Lisbon');
?>
<?php

class Registo {

    //ATRIBUTOS
    //GET SET
    //CONSTRUTOR
    public function __construct() {
        
    }

    //METODOS
    public function inserirDados($idPlanta, $humidadeAr, $humidadeSolo, $temperatura, $rega) {

        //INSTANCIAR DAO REGISTO
        $registoDAO = new RegistoDAO();

        //INSTANCIAR DAO PLANTA
        $plantaDAO = new PlantaDAO();

        //INSTANCIAR DTO REGISTO
        $registoDTO = new RegistoDTO();

        //INSTANCIAR DTO PLANTA
        $plantaDTO = new PlantaDTO();


        //BUSCAR NOME DA PLANTA
        $controlo = $plantaDAO->consultaGenerica(
                "SELECT nome FROM t_plantas WHERE id_planta = ?"
                , $idPlanta);



        //VER ERROS BD
        //BUSCAR TRANSACAO
        $numeroTransacao = array_column($controlo, "numero_transacao");
        $descricaoTransacao = array_column($controlo, "descricao_transacao");

        if ($numeroTransacao[0] !== 0) {
            //CONVERTER JSON
            return json_encode(array(
                "numero_transacao" => $numeroTransacao[0],
                "descricao_transacao" => $descricaoTransacao[0]
                    ), JSON_UNESCAPED_UNICODE);
        } else {
            //PRRENCHER DTO PLANTA
            $plantaDTO->setNome($controlo[0]["nome"]);

            //PREENCHER DTO REGISTO
            $registoDTO->setIdPlanta($idPlanta);
            $registoDTO->setHumidadeAr($humidadeAr);
            $registoDTO->setHumidadeSolo($humidadeSolo);
            $registoDTO->setTemperatura($temperatura);
            $registoDTO->setRega($rega);
            $registoDTO->setData(date("y-m-d H:i:s"));

            //INSERIR REGISTO
            $controlo = $registoDAO->inserir(
                    "INSERT INTO t_registos "
                    . "("
                    . "id_planta,"
                    . " humidade_ar,"
                    . " humidade_solo, "
                    . " temperatura,"
                    . " rega,"
                    . " data_hora)"
                    . " VALUES (?,?,?,?,?,?)"
                    , $registoDTO);
            //RETORNAR RESPOSTA
            if ($controlo["numero_transacao"] != 0) {
                //CONVERTER JSON
                return json_encode(array(
                    "numero_transacao" => $controlo["numero_transacao"],
                    "descricao_transacao" => $controlo["descricao_transacao"]
                        ), JSON_UNESCAPED_UNICODE);
            } else {
                return json_encode($registoDTO->jsonSerialize(), JSON_UNESCAPED_UNICODE);
            }
        }
    }

    public function lerDHT11Planta1() {
        $json = exec("sudo python ../scripts/lerDHT11Planta1.py 2>&1");

        return $json;
    }

    public function lerHumidadeSolo($idPlanta) {
        if ($idPlanta == 1) {
            $json = exec("sudo python ../scripts/lerHumidadeSoloPlanta1.py 2>&1");

            return $json;
        }else{
            $json = exec("sudo python ../scripts/lerHumidadeSoloPlanta2.py 2>&1");

            return $json;
        }
    }

    public function obterRegistos($idPlanta, $dataInicio, $dataFim) {

        //INSTANCIAR DAO REGISTO
        $registoDAO = new RegistoDAO();

        //INSTANCIAR DAO PLANTA
        $plantaDAO = new PlantaDAO();

        //INSTANCIAR DTO REGISTO
        $registoDTO = new RegistoDTO();

        //INSTANCIAR DTO PLANTA
        $plantaDTO = new PlantaDTO();

        //CONTROLO
        $controlo = $registoDAO->consultaGlobal(
                "SELECT
                    t_plantas.nome,
                    t_registos.humidade_ar,
                    t_registos.humidade_solo,
                    t_registos.temperatura,
                    t_registos.data_hora
                FROM t_registos
                INNER JOIN t_plantas
                    ON t_plantas.id_planta = t_registos.id_planta
                WHERE
                    t_registos.id_planta = IFNULL(?, t_registos.id_planta)
                AND
                    (t_registos.data_hora >= IFNULL(?,'1970-01-01') AND t_registos.data_hora <= IFNULL(?,'2999-01-01'))"
                , $idPlanta, $dataInicio, $dataFim
        );

        $numeroTransacao = array_column($controlo, "numero_transacao");
        $descricaoTransacao = array_column($controlo, "descricao_transacao");

        if ($numeroTransacao[0] !== 0) {
            //CONVERTER JSON
            return json_encode(array(
                "numero_transacao" => $numeroTransacao[0],
                "descricao_transacao" => $descricaoTransacao[0]
                    ), JSON_UNESCAPED_UNICODE);
        } else {
            //FAZER UM ARRAY DE MODELOS
            $arrayPlanta = array();

            //PERCORRER RESULTADO E PREENCHER MODELO COM ARRAY
            foreach ($controlo as $chave => $valor) {
                foreach ($valor as $subChave => $subValor) {
                    if ($subChave === "nome") {
                        //PASSAR MODELO PARA ARRAY
                        array_push($arrayPlanta, $valor);
                    }
                }
            }

            //CONVERTER ARRAY PARA JSON
            $json = json_encode($arrayPlanta, JSON_UNESCAPED_UNICODE);
            //RETORNAR JSON
            return $json;
        }
    }

}
