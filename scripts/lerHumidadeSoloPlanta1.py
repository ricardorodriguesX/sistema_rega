import smbus
import time
import sys
import requests
import json


address = 0x48
A0 = 0x40
A1 = 0x41
A2 = 0x42
A3 = 0x43
bus = smbus.SMBus(1)

bus.write_byte(address,A0)
value = bus.read_byte(address)

humidade = value
humidade = (201 - humidade) / 1.01
humidade = int(humidade)
jsonString = {"idPlanta": 1,"solo": humidade};

print json.dumps(jsonString);
