import smbus
import time
import sys
import requests
import json
import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setwarnings(False)
gpio.setup(27, gpio.OUT)
gpio.output(27,0)

address = 0x48
A0 = 0x40
A1 = 0x41
A2 = 0x42
A3 = 0x43
bus = smbus.SMBus(1)

bus.write_byte(address,A1)
value = bus.read_byte(address)


humidade = value
humidade = (humidade * 100) / -255
humidade = humidade + 100


jsonString = {"idPlanta": 2,"solo": humidade};

print json.dumps(jsonString);
gpio.output(27,1)
