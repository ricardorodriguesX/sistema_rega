import sys
import requests
import Adafruit_DHT
import json
import time
import smbus
import RPi.GPIO as gpio

address = 0x48
A0 = 0x40
A1 = 0x41
A2 = 0x42
A3 = 0x43
bus = smbus.SMBus(1)

i = 0;

while True:
    gpio.setmode(gpio.BCM)
    gpio.setwarnings(False)
    gpio.setup(17, gpio.OUT)
    gpio.setup(6, gpio.OUT)
    gpio.output(17,0)
    gpio.output(6,0)
    

    humidity, temperature = Adafruit_DHT.read_retry(11, 5)

    bus.write_byte(address,A0)
    value = bus.read_byte(address)

    time.sleep(15)

    humidity, temperature = Adafruit_DHT.read_retry(11, 5)

    bus.write_byte(address,A0)
    value = bus.read_byte(address)

    humidade = value
    humidade = (humidade * 100) / -45
    humidade = humidade + 100


    payload = {"idPlanta": 1, "temperatura": temperature, "humidadeAr": humidity, "humidadeSolo": humidade};

    r = requests.post("http://localhost/services/InserirDadosPlantaService.php", data=payload);
    
    print r.text
    
    gpio.output(6,1)
    gpio.output(17,1)
    time.sleep(10800)

    
