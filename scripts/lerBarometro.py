import Adafruit_BMP.BMP085 as BMP085
import json
import sys
import requests

sensor = BMP085.BMP085()

pressao = sensor.read_pressure();
altitude = sensor.read_altitude();
pressaoNivelMar = sensor.read_sealevel_pressure();

jsonString = {
    "pressao": pressao,
    "altitude": altitude,
    "pressaoMar": pressaoNivelMar
};

print json.dumps(jsonString);
