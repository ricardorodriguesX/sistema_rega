import RPi.GPIO as gpio
import json
from collections import namedtuple
import time
import sys
import requests

gpio.setmode(gpio.BCM);
gpio.setwarnings(False);
gpio.setup(12, gpio.OUT);
gpio.setup(24, gpio.OUT);

texto = sys.argv[1]

lst = texto.split("_")

id = int(lst[1])
segundos = int(lst[2])

if id == 1:
    gpio.output(12, 1)
    time.sleep(segundos)
    gpio.output(12, 0)
else:
    gpio.output(24, 1)
    time.sleep(segundos)
    gpio.output(24, 0)