import json
from collections import namedtuple
import time
import sys
import requests
import smbus
import RPi.GPIO as gpio


gpio.setmode(gpio.BCM)

gpio.setwarnings(False);

gpio.setup(12, gpio.OUT);

payload = {"idPlanta": 1};


while True:


    r = requests.post("http://localhost/services/ObterHumidadePlantaService.php", data=payload);


    jsonString = r.text


    planta1 = json.loads(jsonString, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

    humidadePlanta1 = planta1.humidadeMin

    address = 0x48
    A0 = 0x40
    A1 = 0x41
    A2 = 0x42
    A3 = 0x43
    bus = smbus.SMBus(1)

    bus.write_byte(address,A0)
    value = bus.read_byte(address)

    humidade = value
    humidade = (201 - humidade) * 1.01
    humidade = int(humidade)

    if humidade < humidadePlanta1:
        gpio.output(12, 1)
    else:
        gpio.output(12, 0)
