<?php 
    $nomePagina = "Rega Manual";
    include_once '../components/header.php'; 
?>
    <main class="main-rega-automatica">
        <div class="container">
            <div class="row pb-20">
                <div class="col-xs-12 col-md-4  col-xs-offset-0 col-md-offset-4 row form-container">
                    <label class="">Planta 1</label>
                    <form class="form-rega-automatica" id="pl1">
                        
                        <div class="form-group row">
                            <label for="humidade" class=" col-xs-7 col-form-label">Segundos de Rega</label>
                            <div class="col-xs-5">
                              <input class="form-control" type="number" value="0" placeholder="Indique a duração de rega em segundos" id="segundos1">
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary col-xs-offset-3" type="submit">Submeter Dados</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4  col-xs-offset-0 col-md-offset-4 row form-container">
                    <label class="">Planta 2</label>
                    <form class="form-rega-automatica" id="pl2">
                        <div class="form-group row">
                            <label for="humidade" class=" col-xs-7 col-form-label">Segundos de Rega</label>
                            <div class="col-xs-5">
                              <input class="form-control" type="number" value="0" placeholder="Indique a duração de rega em segundos" id="segundos2">
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary col-xs-offset-3" type="submit">Submeter Dados</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
   </body>
   <script>
       $(".form-rega-automatica").submit(function(evento){
           evento.preventDefault();
           var idPlanta;
           var segundos;
           
           if($(this).attr("id") === "pl1"){
               idPlanta = 1;
               segundos = $("#segundos1").val();
           }else{
               idPlanta = 2;
               segundos = $("#segundos2").val();
           }
         
           
           $.ajax({
               type:"POST",
               data: {
                   idPlanta: idPlanta,
                   segundos: segundos
               },
               url:"../services/RegaManualService.php"
           }).done(function(resposta){
               console.log(resposta)
           }).fail(function(resposta){
               console.log(resposta)
           })
       })
   </script>
</html>
