<?php 
    $nomePagina = "TESTES";
    include_once '../components/header.php';
?>

<?php
use PiPHP\GPIO\GPIO;
use PiPHP\GPIO\Pin\InputPinInterface;

    $gpio = new GPIO();
    
    $pino = $gpio->getInputPin(18);
    
    // Configure interrupts for both rising and falling edges
$pin->setEdge(InputPinInterface::EDGE_BOTH);

// Create an interrupt watcher
$interruptWatcher = $gpio->createWatcher();

// Register a callback to be triggered on pin interrupts
$interruptWatcher->register($pin, function (InputPinInterface $pin, $value) {
    echo '<h1>TESTE: Pin ' . $pin->getNumber() . ' changed to: ' . $value . PHP_EOL . "</h1>";

    // Returning false will make the watcher return false immediately
    return true;
});

// Watch for interrupts, timeout after 5000ms (5 seconds)
while ($interruptWatcher->watch(5000));
    
    
?>

        <body>
        
        </body>
 </html>