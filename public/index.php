<?php
$nomePagina = "Inicio";
include_once '../components/header.php';
?>
<main>
    <div class="container conteudo_index">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ul class="row text-center charts">
                <div class="carousel-inner">
                    <div class="item active">
                        <h2>Planta1</h2>
                        <li class="col-md-12 justify-content-md-center">
                            <canvas class="planta1"></canvas>
                        </li>
                    </div>
                    <div class="item">
                        <h2>Planta2</h2>
                        <li class="col-md-12 justify-content-md-center">
                            <canvas class="planta2"></canvas>
                        </li>
                    </div>   
                </div>

            </ul>

            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </div>
</main>
<script type="text/javascript">
    var temperatura;
    var humidade;
    var solo1;
    var myChart1;
    var solo2;
    var myChart2;

    $(document).ready(function () {
        var ctx = $(".planta1");
        myChart1 = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Temperatura Ar", "Humidade Ar", "Humidade Solo"],
                datasets: [{
                        label: 'Indicadores Instantaneos',
                        data: [0, 0, 0],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });

        lerDHT11();

        setInterval(function () {
            lerDHT11();

        }, 3600000)

    })


    $(document).ready(function () {
        var ctx = $(".planta2");
        myChart2 = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Temperatura Ar", "Humidade Ar", "Humidade Solo"],
                datasets: [{
                        label: 'Indicadores Instantaneos',
                        data: [0, 0, 0],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });

        lerDHT11();

        setInterval(function () {
            lerDHT11();

        }, 3600000)

    })


    //METODOS ADICIOANIS
    function lerDHT11() {
        $.ajax({
            type: "POST",
            url: "../services/LerDHT11Planta1Service.php"
        }).done(function (resposta) {
            var obj = JSON.parse(resposta);

            temperatura = obj["temperatura"];
            humidade = obj["humidade"];

            lerHumidadeSolo(1);
            lerHumidadeSolo(2);

            setTimeout(function () {
                atualizarGraf(myChart1, temperatura, humidade, solo1);
                atualizarGraf(myChart2, temperatura, humidade, solo2);
            }, 1000)

        }).fail(function (resposta) {

        })
    }

    function lerHumidadeSolo(idPlanta) {
        $.ajax({
            type: "POST",
            url: "../services/LerHumidadeSoloService.php",
            data:{
                idPlanta
            }
        }).done(function (resposta) {
            var obj = JSON.parse(resposta);

            if(obj["idPlanta"] === 1){
                solo1 = obj["solo"];
            }else{
                solo2 = obj["solo"];
            }
            
        }).fail(function (resposta) {

        })
    }

    function atualizarGraf(grafico, temperatura, humidade, solo) {
        grafico.data["datasets"][0]["data"][0] = temperatura;
        grafico.data["datasets"][0]["data"][1] = humidade;
        grafico.data["datasets"][0]["data"][2] = solo;
        grafico.update();
    }



</script>
</body>
</html>
