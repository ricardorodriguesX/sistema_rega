<?php 
    $nomePagina = "Rega Automática";
    include_once '../components/header.php'; 
?>
    <main class="main-rega-automatica">
        <div class="container">
            <div class="row pb-20">
                <div class="col-xs-12 col-md-4  col-xs-offset-0 col-md-offset-4 row form-container">
                    <label class="">Planta 1</label>
                    <form class="form-rega-automatica" id="pl1">
                        
                        <div class="form-group row">
                            <label for="humidade" class=" col-xs-7 col-form-label">Humidade Solo</label>
                            <div class="col-xs-5">
                              <input class="form-control" type="number" value="0" placeholder="Indique a Humidade Minima" id="humidade1">
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary col-xs-offset-3" type="submit">Submeter Dados</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4  col-xs-offset-0 col-md-offset-4 row form-container">
                    <label class="">Planta 2</label>
                    <form class="form-rega-automatica" id="pl2">
                        <div class="form-group row">
                            <label for="humidade" class=" col-xs-7 col-form-label">Humidade Solo</label>
                            <div class="col-xs-5">
                              <input class="form-control" type="number" value="0" placeholder="Indique a Humidade Minima" id="humidade2">
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary col-xs-offset-3" type="submit">Submeter Dados</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
   </body>
   <script>
       $(".form-rega-automatica").submit(function(evento){
           evento.preventDefault();
           var idPlanta;
           var humidade;
           
           if($(this).attr("id") === "pl1"){
               idPlanta = 1;
               humidade = $("#humidade1").val();
           }else{
               idPlanta = 2;
               humidade = $("#humidade2").val();
           }
         
           
           $.ajax({
               type:"POST",
               data: {
                   humidadeSolo: humidade,
                   idPlanta: idPlanta
               },
               url:"../services/MudarHumidadeMinimaService.php"
           }).done(function(resposta){
               console.log(resposta)
           }).fail(function(resposta){
               console.log(resposta)
           })
       })
       
       $(document).ready(function(){
           $.ajax({
               type:"POST",
               data: {
                   idPlanta: 1
               },
               url:"../services/ObterHumidadePlantaService.php"
           }).done(function(resposta){
               var obj = JSON.parse(resposta);
               
               $("#humidade1").val(obj["humidadeMin"])
           }).fail(function(resposta){
               console.log(resposta)
           })
           
           $.ajax({
               type:"POST",
               data: {
                   idPlanta: 2
               },
               url:"../services/ObterHumidadePlantaService.php"
           }).done(function(resposta){
               var obj = JSON.parse(resposta);
               
               $("#humidade2").val(obj["humidadeMin"])
           }).fail(function(resposta){
               console.log(resposta)
           })
       })
   </script>
</html>
