<?php
$nomePagina = "Consulta de Dados";
include_once '../components/header.php';
?>
<div class="container">
    <div class="row">
        <form class="form-consulta col-md-5 col-md-offset-4" id="pl1">

            <div class="form-group row">
                <label for="humidade" class="">ID Planta</label>
                <input class="form-control" type="number" placeholder="Indique Indique o Id da Planta" id="idPlanta" name="idPlanta">
            </div>
            <div class="form-group row">
                <label for="humidade" class="">Data Inicio</label>
                <input class="form-control" type="text" placeholder="Indique a data de inicio" id="dataInicio" name="dataInicio">
            </div>
            <div class="form-group row">
                <label for="humidade" class="">Data Fim</label>
                <input class="form-control" type="text" placeholder="Indique a data de fim" id="dataFim" name="dataFim">
            </div>
            <div class="row">
                <button class="btn btn-primary col-xs-offset-3" type="submit">Submeter Dados</button>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-hover paginated" id="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Planta</th>
                        <th>Humidade Ar</th>
                        <th>Temperatura</th>
                        <th>Humidade Solo</th>
                        <th>Data</th>
                    </tr>
                </thead>
                <tbody id="myTable">

                </tbody>
            </table>   
        </div>
        <div class="paging-nav"></div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $("#dataInicio").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#dataFim").datepicker({ dateFormat: 'yy-mm-dd' });
        $.ajax({
            type: "POST",
            url: "../services/ObterDadosService.php"
        }).done(function (resposta) {
            var obj = JSON.parse(resposta);
            for (var i = 0; i < obj.length; i++) {
                var tr =
                        "<tr>" +
                        "<td>" + i + "</td>" +
                        "<td>" + obj[i]["nome"] + "</td>" +
                        "<td>" + obj[i]["humidade_ar"] + "</td>" +
                        "<td>" + obj[i]["temperatura"] + "</td>" +
                        "<td>" + obj[i]["humidade_solo"] + "</td>" +
                        "<td>" + obj[i]["data_hora"] + "</td>" +
                        "</tr>";
                $("#myTable").append(tr);


            }

            $('#table.paginated').each(function () {
                var currentPage = 0;
                var numPerPage = 10;
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div class="pager"></div>');
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                }
                $pager.insertBefore($table).find('span.page-number:first').addClass('active');
            })


        }).fail(function (resposta) {
            console.log(resposta)
        })



    })

    $(".form-consulta").submit(function (evento) {
        evento.preventDefault();
        var dados = $(this).serialize();
        $("#myTable").empty();
        $(".pager").remove();
        $.ajax({
            type: "POST",
            url: "../services/ObterDadosService.php",
            data:dados
        }).done(function (resposta) {
            var obj = JSON.parse(resposta);
            for (var i = 0; i < obj.length; i++) {
                var tr =
                        "<tr>" +
                        "<td>" + i + "</td>" +
                        "<td>" + obj[i]["nome"] + "</td>" +
                        "<td>" + obj[i]["humidade_ar"] + "</td>" +
                        "<td>" + obj[i]["temperatura"] + "</td>" +
                        "<td>" + obj[i]["humidade_solo"] + "</td>" +
                        "<td>" + obj[i]["data_hora"] + "</td>" +
                        "</tr>";
                $("#myTable").append(tr);


            }

            $('#table.paginated').each(function () {
                var currentPage = 0;
                var numPerPage = 10;
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div class="pager"></div>');
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                }
                $pager.insertBefore($table).find('span.page-number:first').addClass('active');
            })


        }).fail(function (resposta) {
            console.log(resposta)
        })
    })

</script>
