<?php require_once '../controllers/Registo.php';?>
<?php

//POSTS
$idPlanta = isset($_POST["idPlanta"]) && !empty($_POST["idPlanta"]) ? $_POST["idPlanta"] : NULL;
$dataInicio = isset($_POST["dataInicio"]) && !empty($_POST["dataInicio"]) ? $_POST["dataInicio"] : NULL;
$dataFim = isset($_POST["dataFim"]) && !empty($_POST["dataFim"]) ? $_POST["dataFim"] : NULL;

//INSTANCIAR CONTROLADOR
$controlador = new Registo();

//OBTER DADOS
$resposta = $controlador->obterRegistos($idPlanta, $dataInicio, $dataFim);

//RETORNAR RESPPSTA
echo $resposta;