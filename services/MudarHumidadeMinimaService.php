<?php require_once '../controllers/Planta.php';?>
<?php
//POST
$humidadeMinima = $_POST["humidadeSolo"];
$idPlanta = $_POST["idPlanta"];

//INSTANCIAR CONTROLADOR
$controlador = new Planta();

//MUDAR HUMIDADE MINIMA
$resposta = $controlador->mudarHumidade($humidadeMinima, $idPlanta);

//RETORNAR RESPOSTA
echo $resposta;
