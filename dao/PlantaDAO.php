<?php require_once 'BaseDAO.php'; ?>

<?php

class PlantaDAO extends BaseDAO{

    //put your code here
    //ATRIBUTOS
    //
    //GET SET
    //
    //CONSTRUTOR
    public function __construct() {
        parent::__construct();
    }
    
    //METODOS
    public function consultaGenerica($query, $idPlanta){
        //PREPARAR QUERY
        $stmt = parent::prepare($query);
        
        //BIND
        $stmt->bind_param("i", $idPlanta);
        
        //EXECUTAR COM CONTROLO
        $controlo = parent::select($stmt);

        //FECHAR STATMENT
        $stmt->close();

        //RETORNAR CONTROLO
        return $controlo;
    }
    
    public function updateHumidade($query, $humidade, $idPlanta){
        //PREPARAR QUERY
        $stmt = parent::prepare($query);

        //BIND
        $stmt->bind_param("ii", $humidade, $idPlanta);
        
        //EXECUTAR COM CONTROLO
        $controlo = parent::update($stmt);

        //FECHAR STATMENT
        $stmt->close();

        //RETORNAR CONTROLO
        return $controlo;
    }

}
