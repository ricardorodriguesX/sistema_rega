<?php require_once '../config/config.php'; ?>

<?php

class BaseDAO {

    //put your code here
    //ATRIBUTOS
    private $ip_con;
    private $username_con;
    private $bd_con;
    private $pass_con;
    private $conexao;

    //GET SET
    //CONSTRUTOR
    public function __construct() {
        //VAR GLOBAIS BD
        global $ip;
        global $username;
        global $bd;
        global $pass;


        $this->ip_con = $ip;
        $this->username_con = $username;
        $this->bd_con = $bd;
        $this->pass_con = $pass;
    }

    //METODOS
    public function prepare($query) {
        //GLOBALIZAR CONEXAO
        global $conexao;

        //INSTANCIAR CONEXAO
        $conexao = new mysqli(
                $this->ip_con, $this->username_con, $this->pass_con, $this->bd_con
        );

        //RETURNAR PREPARE
        return $conexao->prepare($query);
    }

    //FUNCOES CRUD BASE
    //INSERT C
    public function insert($query) {
        //GLOBALIZAR CONEXAO
        global $conexao;

        //EXECUTAR
        $query->execute();

        //FECHAR CONEXAO
        $conexao->close();

        //ARRAY ERRO E DESCRICAO
        $arrErro = array($query->errno, $query->error);

        //RETORNAR BOOLEANO
        return $arrErro;
    }

    //SELECT R
    public function select($query) {
        //GLOBALIZAR CONEXAO
        global $conexao;

        //DECLARAR ARRAY ASSOCIATIVO DE RESULTADOS
        $arrayResultados = array();

        //EXECUTAR QUERY
        $query->execute() or die($query->error);

        //RESULTADO
        $resultado = $query->get_result();

        while ($row = $resultado->fetch_assoc()) {
            //PUSH PARA ARRAY DE RESULTADOS
            array_push($arrayResultados, $row);
        }

        //FECHAR CONEXAO
        $conexao->close();

        //ADICINAR CHAVE DE ERROS
        $arrayErros = array("numero_transacao" => $query->errno, "descricao_transacao" => $query->error);

        //PUSH PARA ARRAY DE RESULTADOS
        array_push($arrayResultados, $arrayErros);


        //RETORNAR ARRAY ASSOCIATIVO
        return $arrayResultados;
    }

    //UPDATE U
    public function update($query) {
        //GLOBALIZAR CONEXAO
        global $conexao;

        //EXECUTAR QUERY
        $query->execute();
        //FECHAR CONEXAO
        $conexao->close();

        //ARRAY ERRO E DESCRICAO
        $arrErro = array("numero_transacao" => $query->errno, "descricao_transacao" => $query->error);

        //RETORNAR BOOLEANO
        return $arrErro;
    }

    //DELETE D
    public function delete($query) {
        //GLOBALIZAR CONEXAO
        global $conexao;

        //EXECUTAR QUERY
        $query->execute() or die($query->error);

        //FECHAR CONEXAO
        $conexao->close();

        //ARRAY ERRO E DESCRICAO
        $arrErro = array($query->errno, $query->error);

        //RETORNAR BOOLEANO
        return $arrErro;
    }

}
