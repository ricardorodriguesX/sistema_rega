<?php require_once 'BaseDAO.php'; ?>
<?php


class RegistoDAO extends BaseDAO {
    //put your code here
    //ATRIBUTOS
    //
    //GET SET
    //
    //CONSTRUTOR
    public function __construct() {
        parent::__construct();
    }
    
    //METODOS
    public function inserir($query, $registoDTO){
         //Preparar a query SQL:
        $stmt = parent::prepare($query);

        //Fazer bind
        $stmt->bind_param("idddss",
                $registoDTO->getIdPlanta(),
                $registoDTO->getHumidadeAr(),
                $registoDTO->getHumidadeSolo(),
                $registoDTO->getTemperatura(),
                $registoDTO->getRega(),
                $registoDTO->getData());

        //Executar com o "controller":
        $controlo = parent::insert($stmt);

        //Fechar query:
        $stmt->close();

        //Retornar controlo:
        return $controlo;
    }
    
    public function consultaGeneria($query){
        //PREPARAR QUERY
        $stmt = parent::prepare($query);
        
        //EXECUTAR COM CONTROLO
        $controlo = parent::select($stmt);

        //FECHAR STATMENT
        $stmt->close();

        //RETORNAR CONTROLO
        return $controlo;
    }
    
    public function consultaGlobal($query, $idPlanta, $dataInicio, $dataFim){
        //PREPARAR QUERY
        $stmt = parent::prepare($query);
        
        //BIND
        $stmt->bind_param("iss",$idPlanta,$dataInicio,$dataFim);
        
        //EXECUTAR COM CONTROLO
        $controlo = parent::select($stmt);

        //FECHAR STATMENT
        $stmt->close();

        //RETORNAR CONTROLO
        return $controlo;
    }
}
