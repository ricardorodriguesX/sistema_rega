<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegistoDTO
 *
 * @author pi
 */
class RegistoDTO implements JsonSerializable{
    //put your code here
    //ATRIBUTOS
    private $idRegisto;
    private $idPlanta;
    private $humidadeAr;
    private $humidadeSolo;
    private $temperatura;
    private $rega;
    private $data;
    
    //GET SET
    public function getIdRegisto(){
        return $this->idRegisto;
    }
    
    public function setIdRegisto($idRegisto){
        $this->idRegisto = $idRegisto;
    }
    
    public function getIdPlanta(){
        return $this->idPlanta;
    }
    
    public function setIdPlanta($idPlanta){
        $this->idPlanta = $idPlanta;
    }
    
    public function getHumidadeAr(){
        return $this->humidadeAr;
    }
    
    public function setHumidadeAr($humidadeAr){
        $this->humidadeAr = $humidadeAr;
    }

    public function getHumidadeSolo(){
        return $this->humidadeSolo;
    }
    
    public function setHumidadeSolo($humidadeSolo){
        $this->humidadeSolo = $humidadeSolo;
    }
    
    public function getTemperatura(){
        return $this->temperatura;
    }
    
    public function setTemperatura($temperatura){
        $this->temperatura = $temperatura;
    }
    
    public function getRega(){
        return $this->rega;
    }
    
    public function setRega($rega){
        $this->rega = $rega;
    }
    
    public function getData(){
        return $this->data;
    }
    
    public function setData($data){
        $this->data = $data;
    }

    //CONSTRUTOR
    public function __construct() {
        
    }
    
    //METODOS
    public function jsonSerialize() {
        return [
            "idRegisto" => $this->idRegisto,
            "idPlanta" => $this->idPlanta,
            "humidadeAr" => $this->humidadeAr,
            "humidadeSolo" => $this->humidadeSolo,
            "temperatura" => $this->temperatura,
            "rega" => $this->rega,
            "data" => $this->data
        ];
    }

}
