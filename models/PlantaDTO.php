<?php

class PlantaDTO  implements JsonSerializable {
    //put your code here
    //ATRIBUTOS
    private $idPlanta;
    private $nome;
    private $humidadeMin;
    
    //GET SET
    public function getIdPlanta(){
        return $this->idPlanta;
    }
    
    public function setIdPlanta($idPlanta){
        $this->idPlanta = $idPlanta;
    }
    
    public function getNome(){
        return $this->nome;
    }
    
    public function setNome($nome){
        $this->nome = $nome;
    }
    
    public function getHumidadeMin(){
        return $this->humidadeMin;
    }
    
    public function setHumidadeMin($humidadeMin){
        $this->humidadeMin = $humidadeMin;
    }
    //CONSTRUTOR
    public function __construct(){
        
    }

    //METODOS
    public function jsonSerialize() {
        return [
            "idPlanta" => $this->idPlanta,
            "nome" => $this->nome,
            "humidadeMin" => $this->humidadeMin
        ];
    }

}
