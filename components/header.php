
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $nomePagina ?></title>
        <link rel="icon" href="img/favicon.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon"/>
        <script src="libs/jquery/jquery-3.2.1.min.js"></script>
        <script src="libs/jquery/jquery-ui.min.js"></script>
        <script src="libs/tether/js/tether.min.js"></script>
        <link type="text/css" rel="stylesheet" href="libs/tether/css/tether.min.css"/>
        <script src="libs/bootstrap/js/bootstrap.min.js"></script>
        <link type="text/css" rel="stylesheet" href="libs/bootstrap/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="libs/jquery/jquery-ui.min.css"/>
        <link type="text/css" rel="stylesheet" href="style/reset.css"/>
        <link type="text/css" rel="stylesheet" href="style/style.css"/>
        <script src="scripts/validacoes.js"></script>
        <script src="scripts/geral.js"></script>
        <script src="libs/paging.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>

    </head>
    <body>
        <header>
            <nav class="navbar">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="hidden-sm hidden-xs navbar-brand" href="index.php"><img class="img-logo" src="img/favicon.png"/></a>
                    </div>
                    <div class="">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="rega_automatica.php">Rega Automática</a></li>
                            <li><a href="rega_manual.php">Rega Manual</a></li>
                            <li><a href="consulta.php">Consultar Dados</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="container">
            <h3 class="nomePagina"><?php echo $nomePagina ?></h3>
        </div>

